/*!
 * Example use case:
 * Need to scroll and show a hidden item within the FAQ section.
*/

document.addEventListener("DOMContentLoaded", () => {
  // A leapTrigger is an anchor link that targets a potentially hidden element.
  const leapTriggers = document.querySelectorAll('a[data-leap-trigger][href^="#"]');
  // Add click events to each leapTrigger, then we can toggle display properties and scroll.
  leapTriggers.forEach((trigger) => {
    const href = trigger.getAttribute('href').substring(1);
    const targetElement = document.getElementById(href);
    if (targetElement) {
      trigger.addEventListener('click', (event) => {
        // Stop default browser scrolling
        event.preventDefault();

        targetElement.closest('.leap-target').setAttribute('open', '');
        targetElement.scrollIntoView();
      });
    }
  });
});
